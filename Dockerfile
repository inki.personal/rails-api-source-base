FROM ruby:3.0.1-slim-buster as builder
LABEL maintainer="Vuong Khai Ha <inki.personal@gmail.com>"

# Set Timezone
ARG TZ
ENV TZ $TZ

# Install core libraries
RUN apt-get update -qq && \
    apt-get install -y  --no-install-recommends build-essential shared-mime-info \
                                                gcc g++ git curl apt-transport-https wget \
                                                bash openssl file tzdata cron \
                                                libmagic-dev libffi-dev default-libmysqlclient-dev vim nano && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set caching paths
ENV BUNDLE_PATH /inki_api/gems
ENV BUNDLE_HOME /inki_api/gems
ENV GEM_PATH /inki_api/gems
ENV GEM_HOME /inki_api/gems
ENV PATH /inki_api/gems/bin:$PATH

WORKDIR /inki_api/$USER
