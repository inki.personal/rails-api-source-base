# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users, unsigned: true do |t|
      t.string   :username, null: false
      t.string   :password_digest
      t.string   :email, null: false
      t.integer  :role, null: false, unsigned: true, limit: 1, default: 0
      t.string   :refresh_token
      t.datetime :refresh_token_expired_at
      t.integer  :availability, unsigned: true, limit: 1, default: 1
      t.datetime :deleted_at
      t.timestamps
    end

    create_table :user_profiles, unsigned: true do |t|
      t.belongs_to :user, unsigned: true
      t.string     :first_name
      t.string     :last_name
      t.integer    :gender, unsigned: true, limit: 1
      t.date       :birthdate
      t.string     :address
    end

    add_index :users, :username, unique: true
    add_index :users, :email, unique: true
    add_index :users, :refresh_token
  end
end
