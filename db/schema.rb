# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_13_093656) do

  create_table "user_profiles", id: { type: :bigint, unsigned: true }, charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "user_id", unsigned: true
    t.string "first_name"
    t.string "last_name"
    t.integer "gender", limit: 1, unsigned: true
    t.date "birthdate"
    t.string "address"
    t.index ["user_id"], name: "index_user_profiles_on_user_id"
  end

  create_table "users", id: { type: :bigint, unsigned: true }, charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "username", null: false
    t.string "password_digest"
    t.string "email", null: false
    t.integer "role", limit: 1, default: 0, null: false, unsigned: true
    t.string "refresh_token"
    t.datetime "refresh_token_expired_at"
    t.integer "availability", limit: 1, default: 1, unsigned: true
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["refresh_token"], name: "index_users_on_refresh_token"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
