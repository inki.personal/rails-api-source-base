# Be sure to restart your server when you modify this file.

# Declare all customized constant data
Rails.application.configure do
  config.settings = config_for(:settings)
  config.responses = config_for(:responses)
end
