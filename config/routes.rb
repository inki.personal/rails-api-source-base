Rails.application.routes.draw do
  defaults format: :json do
    namespace :v1 do
      namespace :public do
        resource :registrations, only: %i[create]
        resource :sessions, only: %i[create update]
      end

      namespace :private do
        resource :account, only: %i[show] do
          resource :profile, only: %i[show update]
        end
      end
    end
  end
end
