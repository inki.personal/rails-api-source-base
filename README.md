# BUILDING ENVIRONMENT
- Create docker network for nginx connecting
```
docker network create nginx_network
```
- Create docker network for containers connecting (You do not need to run this command)
```
docker network create -d bridge --attachable --internal <network-name>
```
- Run this command to start server
```
docker-compose up --build -d
```

# FOR CALLING API
- You must add `Internal-Token` into `headers` whose content is same as `INTERNAL_TOKEN` in `.env` file.
- The `Authorization` in `headers` must in the format: `Bearer <access_token>`

# FOR ISSUE HANDLING
- Show all processes listen <port>
```
sudo netstat -nlp | grep <port>
```
- Delete everything, settings
```
docker-compose rm -v
```
