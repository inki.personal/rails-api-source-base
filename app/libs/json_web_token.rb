# frozen_string_literal: true

class JsonWebToken
  SECRET_KEY = Rails.application.secrets.secret_key_base.to_s
  DURATIONS = Rails.configuration.settings.dig(:duration, :jwt)

  # Encode data to jwt token
  def self.encode(payload, exp_date: nil, exp_type: :default)
    payload[:exp] = (exp_date.presence || DURATIONS[exp_type].hours.from_now).to_i
    JWT.encode(payload, SECRET_KEY)
  end

  # Decode jwt token
  def self.decode(token)
    decoded = JWT.decode(token, SECRET_KEY)[0]
    HashWithIndifferentAccess.new decoded
  rescue JWT::ExpiredSignature
    raise ActionError.new :token, :invalid_or_expired
  end
end
