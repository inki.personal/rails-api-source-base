# frozen_string_literal: true

class ActionError < StandardError
  attr_accessor :data, :record, :http_code

  RESPONSE_CODES = Rails.configuration.responses
  DEFAULT_TARGET_ERROR_TYPE = %i[undefined undefined].freeze

  def initialize(*target_error_type, **options)
    if options[:object].present?
      build_data_from_object(options[:object])
    else
      build_data_from_attributes(*target_error_type, default: options[:default])
    end
  end

  def self.===(_)
    true
  end

  private

  def build_data_from_object(error_record)
    self.record = error_record.try(:record)
    if error_record.instance_of?(::ActionError)
      self.data = error_record.data
      self.http_code = error_record.http_code
    else
      build_data_from_attributes(:framework, error_record.class.name.underscore)
    end
  end

  def build_data_from_attributes(*target_error_type, default: [])
    target, error_type = target_error_type
    codes = RESPONSE_CODES.dig(target, error_type.to_sym)
    return build_response_data(target, error_type, codes) if codes.present?

    target, error_type = default.presence || DEFAULT_TARGET_ERROR_TYPE
    build_response_data(target, error_type, RESPONSE_CODES.dig(target, error_type.to_sym))
  end

  def build_response_data(target, error_type, codes)
    self.data = {
      status: false,
      code: codes[:code],
      message: I18n.t("action_error.#{target}.#{error_type}", default: '')
    }
    data.merge!(data: record_errors_data) if record.present?
    self.http_code = codes[:http_code]
  end

  def record_errors_data
    record.errors.details.keys.reduce({}) do |result, k|
      result.update k => record.errors.full_messages_for(k)
    end
  end
end
