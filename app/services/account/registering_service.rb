# frozen_string_literal: true

class Account::RegisteringService
  include ActiveModel::Model
  include ActiveModel::Attributes

  attr_reader :user

  attribute :username, :string
  attribute :email, :string
  attribute :password, :string
  attribute :password_confirmation, :string

  validates_presence_of :password, :password_confirmation

  def execute!
    default_attributes_is_invalid = (@user = User.new attributes).invalid?
    if invalid? || default_attributes_is_invalid
      errors.merge! @user.errors
      raise ActiveRecord::RecordInvalid, self
    end
    @user.generate_refresh_token
    @user.save validate: false
  end
end
