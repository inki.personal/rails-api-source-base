# frozen_string_literal: true

class Account::AuthenticatingService
  attr_reader :user

  def initialize(params)
    @params = params
  end

  def verify_for_signing_in!
    @user = User.find_by email: @params[:email]
    raise ActionError.new(:auth, :email_or_password_invalid) unless @user&.authenticate(@params[:password])

    @user.generate_refresh_token
  end

  def verify_for_refreshing_access_token!
    @user = User.find_by refresh_token: @params[:refresh_token]
    raise ActionError.new(:token, :invalid_or_expired) if @user.blank?
  end
end
