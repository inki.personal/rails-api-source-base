# frozen_string_literal: true

module Responsable
  DEFAULT_RESPONSE_ACTIONS = %i[requested created updated destroyed].freeze

  DEFAULT_RESPONSE_ACTIONS.each do |response_action|
    define_method :"response_#{response_action}" do |
      message: "request.#{response_action}_successfully",
      code: [:success, response_action],
      data: nil
    |
      render json: ({
        status: true,
        code: Rails.configuration.responses.dig(*code),
        message: I18n.t("announcements.#{message}")
      }.tap { |temp_data| temp_data.update(data: data) if data.present? })
    end
  end
end
