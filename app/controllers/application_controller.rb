class ApplicationController < ActionController::API
  before_action :authenticate_internal_request!

  include Responsable

  helper_method :current_user

  rescue_from Exception do |e|
    error_object = ActionError.new object: e
    render json: error_object.data, status: error_object.http_code
  end

  protected

  ###### BEGIN: AUTHENTICATION ########
  # Checking request is internal request
  def authenticate_internal_request!
    raise ActionError.new(:token, :not_internal) if request.headers['Internal-Token'] != ENV['INTERNAL_TOKEN']
  end

  def authenticate_user!
    raise ActionError.new(:auth, :unauthorized) if current_user.blank?
  end

  ###### BEGIN: HELPER METHODS ########
  # Access token from headers
  def access_token
    @access_token ||= begin
      authorization = request.headers['Authorization']
      raise ActionError.new(:auth, :unauthorized) if authorization.blank?

      authorization.split.last
    end
  end

  # Get user depends on access token
  def current_user
    @current_user ||= User.find_by id: JsonWebToken.decode(access_token)[:user_id]
  rescue StandardError
    raise ActionError.new(:token, :invalid_or_expired)
  end
end
