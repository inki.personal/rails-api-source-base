# frozen_string_literal: true

class V1::Public::SessionsController < PublicController
  # Action: Sign In
  def create
    service = Account::AuthenticatingService.new signing_in_params
    service.verify_for_signing_in!
    @user = service.user
  end

  # Action: Refresh access_totken
  def update
    service = Account::AuthenticatingService.new refresh_token_params
    service.verify_for_refreshing_access_token!
    @user = service.user
  end

  private

  def signing_in_params
    params.permit :email, :password
  end

  def refresh_token_params
    params.permit :refresh_token
  end
end
