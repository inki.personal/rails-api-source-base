# frozen_string_literal: true

class V1::Public::RegistrationsController < PublicController
  # Action: Create user
  def create
    (service = Account::RegisteringService.new registering_params).execute!
    @user = service.user
  end

  private

  def registering_params
    params.permit :username, :email, :password, :password_confirmation
  end
end
