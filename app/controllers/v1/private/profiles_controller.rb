# frozen_string_literal: true

class V1::Private::ProfilesController < PrivateController
  # Action: Profile
  def show; end

  def update
    current_user.profile.update! updating_profile_params
    response_updated
  end

  private

  def updating_profile_params
    params.require(:profile).permit(:first_name, :last_name, :gender, :birthdate, :address)
  end
end
