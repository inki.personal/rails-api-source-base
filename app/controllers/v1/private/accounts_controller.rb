# frozen_string_literal: true

class V1::Private::AccountsController < PrivateController
  # Action: Me
  def show; end
end
