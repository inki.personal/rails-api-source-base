# frozen_string_literal: true

class User < ApplicationRecord
  ###### Inclusion ########
  has_secure_password

  include LogicalDelete

  ###### Enums ########
  enum role: { standard: 0, moderator: 1, administrator: 9 }

  ###### Associations ########
  has_one :profile, class_name: 'User::Profile', foreign_key: :user_id, inverse_of: :user

  ###### Validations ########
  validates_presence_of :username, :email, :password
  validates_length_of :username, :email, :password, maximum: 255
  validates :email, format: { with: Rails.configuration.settings.dig(:regex, :email) },
                    uniqueness: true, if: -> { email.present? }
  validates :username, format: { with: Rails.configuration.settings.dig(:regex, :slug), message: :slug_format },
                       uniqueness: true, if: -> { username.present? }

  ###### Nested attributes ########
  accepts_nested_attributes_for :profile, update_only: true

  ###### Callbacks ########
  after_create :create_profile

  ###### Getters ########
  def access_token(generate_new: false)
    return @access_token if !generate_new && @access_token.present?

    expires_time = Rails.configuration.settings.dig(:duration, :jwt, :access_token).hours.from_now
    @access_token = {
      content: JsonWebToken.encode({ user_id: id }, exp_date: expires_time),
      expires_at: expires_time
    }
  end

  def refresh_token_available?
    refresh_token_expired_at.present? && (refresh_token_expired_at > Time.zone.now)
  end

  ###### Setters ########
  def generate_refresh_token
    assign_attributes(
      refresh_token: "#{SecureRandom.alphanumeric(32)}#{id}",
      refresh_token_expired_at: Rails.configuration.settings
                                     .dig(:duration, :jwt, :refresh_token)
                                     .hours.from_now
    )
    save validate: false
  end
end
