# frozen_string_literal: true

class User::Profile < ApplicationRecord
  ###### Constants ########
  BIRTHDATE_YEAR_GAPS = Rails.configuration.settings.dig(:years_gap, :birthdate)

  ###### Enums ########
  enum gender: { female: 0, male: 1, lgbt: 2 }

  ###### Associations ########
  belongs_to :user, class_name: 'User', foreign_key: :user_id, inverse_of: :profile

  ###### Validations ########
  validates_length_of :first_name, :last_name, :address, maximum: 255
  validates_inclusion_of :gender, in: genders.keys, if: -> { gender.present? }
  validates :birthdate, date_range: {
    greater_than_or_equal_to: Date.current - BIRTHDATE_YEAR_GAPS[:maximum].years,
    less_than_or_equal_to: Date.current - BIRTHDATE_YEAR_GAPS[:minimum].years
  }, if: -> { birthdate.present? }
end
