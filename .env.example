############ SERVER SETTINGS ############
### USE FOR `nginx-proxy`
# The domain of application. Example: vuongkhaiha.com
VIRTUAL_HOST=localhost

# The port which application service exposes
VIRTUAL_PORT=3000

# The application domain which letsencrypr will generate for the SSL certificates (https)
LETSENCRYPT_HOST=localhost

# The email address you want to be associated with the certificates
LETSENCRYPT_EMAIL=inki.personal@gmail.com

### CREDENTIALS
# The master key is used to generate application credentials.
# This should not be show the real value of this
# Remote the /config/credentials.yml.enc and /config/master.key if you change this value
SECRET_KEY_BASE=theExampleRailsMasterKey00_11kkapq_

# The application environment: (development/test/production)
RAILS_ENV=development

# The application port
RAILS_PORT=3000

# The minimum threads application will be running in
RAILS_MIN_THREADS=5

# The maximum threads application will be running in
RAILS_MAX_THREADS=5

# This setting will set the application in clusted mode or single mode
# The value should be equals to the server's core
RAILS_WEB_CONCURRENCY=0

# The default application process id
PIDFILE=tmp/pids/server.pid

# Crontab log file path
CRON_LOG_PATH=log/crontab.log

# The application timezome
TZ=Asia/Ho_Chi_Minh
########## END SERVER SETTING ###########

############ SYSTEM SETTINGS ############
# The database encoding
DATABASE_ENCODING=utf8mb4

# The database username
DATABASE_USERNAME=root

# The database user's password
DATABASE_PASSWORD=password

# The host of database
# - If you run mysql service by docker. The value must be the name of service (`mysql`).
# - If you wanna connect application to installed mysql service which not built by docker,
#   The value must be the ip or domain of mysql
DATABASE_HOST=mysql

# The port of database
DATABASE_PORT=3306

# The name of database
DATABASE_NAME=inki_api

# The maximum connection from application to mysql
# It should be the result of RAILS_WEB_CONCURRENCY * RAILS_MAX_THREADS
DATABASE_MAX_POOL=5
######### END: SYSTEM SETTINGS ##########

############# APP SETTINGS ##############
# PUBLIC STATIC ASSETS
RAILS_SERVE_STATIC_FILES=true

### CORS
# The accepted requests source
# - Use * for allowing requests from every client
# - Or domain, ip for specified address
# The domains, ips list must be inline with a comma
# Example: localhost, 127.0.0.1
AVAILABLE_REQUEST_DOMAINS=*

### TOKENS
# The duration of tokens. Unit is hours

# Default token duration
DEFAULT_TOKEN_DURATION=24

# Access token duration
ACCESS_TOKEN_DURATION=4

# Refresh token duration
REFRESH_TOKEN_DURATION=168

### LANGUAGES
# Available languages should be fixed position/value. Example:
# en: 0
# ja: 1
# It's same as ['en', 'ja'] or en|ja
# Current application supports `en` languages.
# If you want to use other language, please translate all files inside config/locales/ folder.
AVAILABLE_LANGUAGES=vi|en
DEFAULT_LOCALE=vi

### The internal request token
# Every request from client must include this token in header
# If you change it, you must change the token inside client
INTERNAL_TOKEN=inki@musicTranslation
########### END APP SETTINGS ############
