#############################
# CORE BASE
#############################
source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.1'
gem 'rails', '~> 6.1.3', '>= 6.1.3.2'
gem 'mysql2', '~> 0.5'
gem 'puma', '~> 5.0'
gem 'bootsnap', '>= 1.4.4', require: false
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

#############################
# AUTHENTICATION / SECURITY
#############################
gem 'bcrypt', '~> 3.1.7'
gem 'rack-cors'
gem 'jwt'

#############################
# ENGINES
#############################
gem 'ransack'
gem 'pagy'
gem 'jb'

#############################
# DEVELOPMENT
#############################
group :development, :test do
  gem 'faker', :git => 'https://github.com/faker-ruby/faker.git', :branch => 'master'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'dotenv-rails'
  gem 'pry-byebug'
end

group :development do
  gem 'listen', '~> 3.3'
  gem 'letter_opener'
  gem 'bullet'
  gem 'rubocop', require: false
end
