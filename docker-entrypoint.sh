#!/bin/bash
# Exit on fail
set -e

# Bundle install
bundle install --jobs `expr $(cat /proc/cpuinfo | grep -c "cpu cores") - 1` --retry 2
# bundle install

# Migrate
bundle exec rails db:create db:migrate

# Remove puma pid if existed
if [ -f tmp/pids/server.pid ]; then
  rm tmp/pids/server.pid
fi

### FOR: Production ###
if [ "$RAILS_ENV" == "production" ]; then
  EDITOR=$SECRET_KEY_BASE bundle exec rails credentials:edit
fi

# Run rails server
bundle exec rails s --port=$RAILS_PORT -b 0.0.0.0 --environment=$RAILS_ENV

# Finally call command issued to the docker service
exec "$@"
